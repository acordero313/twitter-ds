import java.io.IOException;
import java.util.List;
import java.text.ParseException;

public class Main {

	public static void main(String[] args) throws ParseException, IOException {
		List<UserLogin> users = UserLogin.parseCsv("logins_data.csv");
		List<EphemeralTweet> tweets = EphemeralTweet.parseCsv("ephemeral_tweets_data.csv");
		
		Analyses.uniqueTweetUsers(tweets);
		Analyses.tweetsPerUser(users, tweets, "user_tweet_volume.csv");
		Analyses.tweetsPerDay(tweets, "tweets_per_day.csv");
		Analyses.clientsPerUser(users, "clients_per_user.csv");
		Analyses.averageClientsPerUserPerCountry(users, 
				"average_clients_per_user_per_country.csv");
	}
}
