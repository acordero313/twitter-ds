import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class EphemeralTweet {
	private LocalDateTime createdDateTime;
	private LocalDateTime deletedDateTime;
	private int tweetId;
	private int userId;
	
	public LocalDateTime getCreatedTweetDateTime() {
		return this.createdDateTime;
	}
	
	public LocalDateTime getDeletedTweetDateTime() {
		return this.deletedDateTime;
	}
	
	public int getTweetId() {
		return this.tweetId;
	}
	
	public int getUserId() {
		return this.userId;
	}	
	
	/**
	 * Takes in a CSV file and parses it into a list of Ephemeral Tweets
	 * 
	 * @param path file name of CSV file to be parsed
	 * @return list of ephemeral tweet instances
	 * @throws IOException if file cannot be found
	 */
	public static List<EphemeralTweet> parseCsv(String path) throws IOException {
		BufferedReader fileReader = new BufferedReader(new FileReader(path));

		// Read to skip the header
		fileReader.readLine();
		
		String line = fileReader.readLine();
		
		List<EphemeralTweet> tweets = new ArrayList<EphemeralTweet>();
		
		while (line != null) {
			EphemeralTweet tweet = parseCsvLine(line);
			tweets.add(tweet);
			
			line = fileReader.readLine();
		}
		
		fileReader.close();
		
		return tweets;
	}
	
	/**
	 * Parses CSV row into ephemeral tweet object
	 * 
	 * @param line CSV row containing ephemeral tweet
	 * @return ephemeral tweet object containing attributes
	 */
	private static EphemeralTweet parseCsvLine(String line) {
		String[] splitLine = line.split(",");
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy H:mm");
		
		EphemeralTweet tweet = new EphemeralTweet();
		
		tweet.createdDateTime = LocalDateTime.parse(splitLine[1], formatter);
		tweet.deletedDateTime = LocalDateTime.parse(splitLine[2], formatter);
		tweet.tweetId = Integer.parseInt(splitLine[3]);
		tweet.userId = Integer.parseInt(splitLine[4]);
		
		return tweet;
	}
}
