import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Analyses {
	
	/**
	 * Calculates the number of unique users that used the ephemeral tweet feature.
	 * 
	 * @param tweets individual ephemeral tweets
	 */
	public static void uniqueTweetUsers(List<EphemeralTweet> tweets) {
		Set<Integer> uniqueUsers = new HashSet<Integer>();
		
		for (EphemeralTweet tweet : tweets) {
			uniqueUsers.add(tweet.getUserId());
		}
		
		System.out.printf("Unique Tweet Users: %d\n\n", uniqueUsers.size());
	}
	
	/**
	 * Calculates the number of users that tweet at a certain volume. For example, you would be
	 * able to answer how many users tweet more than a single time. Outputs in CSV format.
	 * 
	 * @param users individual user login instances
	 * @param tweets individual ephemeral tweets
	 * @throws IOException unable to write out CSV file
	 */
	public static void tweetsPerUser(List<UserLogin> users, List<EphemeralTweet> tweets, 
			String outputPath) throws IOException {
		
		Map<Integer, Integer> numUserTweets = new HashMap<Integer, Integer>();
		
		for (UserLogin user : users) {
			numUserTweets.put(user.getUserId(), 0);
		}
		
		for (EphemeralTweet tweet : tweets) {
			int tweetCount = numUserTweets.get(tweet.getUserId());
			numUserTweets.put(tweet.getUserId(), tweetCount+1);
		}
		
		Map<Integer, Integer> numTweetsToUserCount = new HashMap<Integer, Integer>();
		
		for (int userId : numUserTweets.keySet()) {
			int userTweetCount = numUserTweets.get(userId);
			
			int currentUserCount = numTweetsToUserCount.getOrDefault(userTweetCount, 0);
			numTweetsToUserCount.put(userTweetCount, currentUserCount+1);
		}

		Csv outputCsv = new Csv("tweet_count,user_count");
		
		for (int tweetVolume : numTweetsToUserCount.keySet()) {
			int userCount = numTweetsToUserCount.get(tweetVolume);
			
			String tweetVolumeCount = String.format("%d,%d", tweetVolume, userCount);
			
			outputCsv.addRow(tweetVolumeCount);
		}
		
		outputCsv.writeToFile(outputPath);
	}
	
	/**
	 * Calculates the number of ephemeral tweets on a given day. Outputs in CSV format.
	 * 
	 * @param tweets individual ephemeral tweets
	 * @throws IOException unable to write out CSV file
	 */
	public static void tweetsPerDay(List<EphemeralTweet> tweets, String outputPath) throws IOException {
		int[] days = new int[30];
		
		for (EphemeralTweet tweet : tweets) {
			int dayIndex = tweet.getCreatedTweetDateTime().getDayOfMonth() - 1;
			days[dayIndex]++;
		}
		
		Csv outputCsv = new Csv("day_of_month,tweet_count");
		
		for (int dayIndex = 0; dayIndex < days.length; dayIndex++) {
			String tweetsPerDay = String.format("%d,%d", dayIndex+1, days[dayIndex]);
			
			outputCsv.addRow(tweetsPerDay);
		}
		
		outputCsv.writeToFile(outputPath);
	}
	
	/**
	 * Calculates the number of client applications per unique user. Outputs in CSV format.
	 * 
	 * @param users individual user login instances
	 * @throws IOException unable to write out CSV file
	 */
	public static void clientsPerUser(List<UserLogin> users, String outputPath) throws IOException {
		Map<Integer, Set<String>> userClientApps = new HashMap<Integer, Set<String>>();
		
		for (UserLogin user : users) {
			if (!userClientApps.containsKey(user.getUserId())) {
				userClientApps.put(user.getUserId(), new HashSet<String>());
			}
			
			userClientApps.get(user.getUserId()).add(user.getClientApp());
		}
		
		Csv outputCsv = new Csv("user_id,num_client_apps");
		
		for (int userId : userClientApps.keySet()) {
			int numClients = userClientApps.get(userId).size();
			String numClientsPerUser = String.format("%d,%d", userId, numClients);
			
			outputCsv.addRow(numClientsPerUser);
		}
		
		outputCsv.writeToFile(outputPath);
	}
	
	/**
	 * Calculates the average number of client applications per user per country. Outputs in CSV
	 * format.
	 * 
	 * @param users individual user login instances
	 * @throws IOException unable to write out CSV file
	 */
	public static void averageClientsPerUserPerCountry(List<UserLogin> users, String outputPath) throws IOException {
		Map<Integer, Integer> clientApps = countClientAppsPerUser(users);
		
		Map<Integer, Set<String>> countryOrigin = originCountryPerUser(users);
		
		Map<String, int[]> totalClientAppsPerCountry = new HashMap<String, int[]>();
		
		for (UserLogin user : users) {
			totalClientAppsPerCountry.put(user.getUserCountry(), new int[2]);
		}
		
		for (int userId : countryOrigin.keySet()) {
			Set<String> currCountries = countryOrigin.get(userId);
			int currUniqueClientAppCount = clientApps.get(userId);
			
			for (String country : currCountries) {
				int[] totalRunningCount = totalClientAppsPerCountry.get(country);
				totalRunningCount[0] += currUniqueClientAppCount;
				totalRunningCount[1] += 1;
				totalClientAppsPerCountry.put(country, totalRunningCount);
			}
		}
		
		Csv outputCsv = new Csv("user_country_origin,average_client_apps_per_user_per_country");
		
		Map<String, Integer> averageClientApp = new HashMap<String, Integer>();
		
		for (String country : totalClientAppsPerCountry.keySet()) {
			int[] totalRunningCount = totalClientAppsPerCountry.get(country);
			
			int average = totalRunningCount[0] / totalRunningCount[1];
			averageClientApp.put(country, average);
			
			String averageClientApps = String.format("%s,%d", country, average);
			
			outputCsv.addRow(averageClientApps);
		}
		
		outputCsv.writeToFile(outputPath);
	}
	
	/**
	 * Calculates the total number of client applications per unique user.
	 * 
	 * @param users individual user login instances
	 * @return a mapping of a unique user to the number of client applications specific to
	 * that user
	 */
	private static Map<Integer, Integer> countClientAppsPerUser(List<UserLogin> users) {
		Map<Integer, Set<String>> clientApps = new HashMap<Integer, Set<String>>();
			
		for (UserLogin user : users) {
			clientApps.put(user.getUserId(), new HashSet<String>());
		}
			
		for (UserLogin user : users) {
			Set<String> currClientAppList = clientApps.get(user.getUserId());
			currClientAppList.add(user.getClientApp());
			clientApps.put(user.getUserId(), currClientAppList);
		}
			
		Map<Integer, Integer> numClientAppsPerUser = new HashMap<Integer, Integer>();
			
		for (Integer userId : clientApps.keySet()) {
			int numClientApp = clientApps.get(userId).size();
			numClientAppsPerUser.put(userId, numClientApp);
		}
			
		return numClientAppsPerUser;
	}
	
	/**
	 * Maps user to its country of origin
	 * 
	 * @param users individual user login instances
	 * @return a mapping of unique users to its country of origin
	 */
	private static Map<Integer, Set<String>> originCountryPerUser(List<UserLogin> users) {
		Map<Integer, Set<String>> userToOriginCountry = new HashMap<Integer, Set<String>>();
		
		for (UserLogin user : users) {
			if (!userToOriginCountry.containsKey(user.getUserId())) {
				userToOriginCountry.put(user.getUserId(), new HashSet<String>());
			}
			
			userToOriginCountry.get(user.getUserId()).add(user.getUserCountry());
		}
		
		return userToOriginCountry;
	}
}
