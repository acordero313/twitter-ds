import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class UserLogin {
	private int userId;
	private String country;
	private String clientApp;
	private LocalDate logInDate;
	
	public int getUserId() {
		return this.userId;
	}
	
	public String getUserCountry() {
		return this.country;
	}
	
	public String getClientApp() {
		return this.clientApp;
	}
	
	public LocalDate getUserLogInDate() {
		return this.logInDate;
	}
	
	/**
	 * Takes in CSV file and parses it into a list of users
	 * 
	 * @param path file name of CSV to be parsed
	 * @return list of user login instances
	 * @throws IOException if file cannot be found
	 */
	public static List<UserLogin> parseCsv(String path) throws IOException {
		BufferedReader fileReader = new BufferedReader(new FileReader(path));
		
		// Read to skip the header
		fileReader.readLine();
		
		String line = fileReader.readLine();
		
		List<UserLogin> users = new ArrayList<UserLogin>();
		
		while (line != null) {
			UserLogin user = parseCsvLine(line);
			users.add(user);
			
			line = fileReader.readLine();
		}
		
		fileReader.close();
		
		return users;
	}
	
	/**
	 * Parses CSV row into user login object
	 * 
	 * @param line CSV row containing user login instances
	 * @return user login object containing attributes
	 */
	private static UserLogin parseCsvLine(String line) {
		String[] splitLine = line.split(",");
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		UserLogin user = new UserLogin();
		
		user.userId = Integer.parseInt(splitLine[1]);
		user.country = splitLine[2];
		user.clientApp = splitLine[3];
		user.logInDate = LocalDate.parse(splitLine[4], formatter);

		return user;
	}
}
