import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Csv {
	private String header;
	private List<String> rows;
	
	public Csv(String header) {
		this.header = header;
		this.rows = new ArrayList<String>();
	}
	
	public void addRow(String row) {
		this.rows.add(row);
	}
	
	/**
	 * Writes out CSV file
	 * 
	 * @param path file name of CSV file to be written to
	 * @throws IOException unable to write out CSV file
	 */
	public void writeToFile(String path) throws IOException {
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(path));
		
		fileWriter.write(this.header);
		fileWriter.newLine();
		
		for (String row : this.rows) {
			fileWriter.write(row);
			fileWriter.newLine();
		}
		
		fileWriter.close();
	}
}
